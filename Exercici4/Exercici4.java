// Copyright (C) Xavi Álvarez. All rights reserved.
package Exercici4;
import Keyboard.*;

public class Exercici4 {

	public static void main(String[] args) {
		
		System.out.println("Classificació de l'IMC");
		System.out.println("------------- -- -----");
		System.out.print("Quin és el teu pes (en Kg.)?: ");
		double w = Keyboard.readDouble();
		System.out.print("Quina és la teva alçada (en m.)?: ");
		double h = Keyboard.readDouble();
		System.out.println();
		double imc = w / Math.pow(h, 2);
		System.out.println("El teu IMC és " + imc);
		
		if(imc < 18) {
			System.out.println("això indica INFRAPÈS.");
		} else if(imc >= 18 && imc <= 25) {
			System.out.println("això vol dir que és NORMAL.");
		} else {
			System.out.println("això indica OBESITAT.");
		}
		
	}
	
}
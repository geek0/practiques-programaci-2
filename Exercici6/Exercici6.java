// Copyright (C) Xavi Álvarez. All rights reserved.
package Exercici6;

public class Exercici6 {

	public static void main(String[] args) {
		
		System.out.println("Classificació de l'IMC");
		System.out.println("------------- -- -----");
		double w = Math.random()*234+3; // afegim límits.
		double h = Math.random()*2.5+0.4; // afegim límits.
		double p = Math.rint(w*100)/100;
		double a = Math.rint(h*100)/100;
		System.out.println("El pes generat aleatoriament ha estat [3,234]: "+p);
		System.out.println("L'alçada generada aleatoriament ha estat [0.4,2.5]: "+a);
		System.out.println();
		double imc = w / Math.pow(h, 2);
		System.out.println("El teu IMC és " + imc);
		
		if(imc < 18) {
			System.out.print("això indica INFRAPÈS");
			if(imc >= 17) {
				System.out.println(" MODERAT.");
			} else if(imc < 17 && imc >= 16) {
				System.out.println(" CONSIDERABLE.");
			} else {
				System.out.println(" SEVER.");
			}
		} else if(imc >= 18 && imc <= 25) {
			System.out.println("això vol dir que és NORMAL.");
		} else {
			System.out.print("això indica OBESITAT");
			if(imc > 25 && imc <= 30) {
				System.out.println(" LLEU.");
			} else if(imc > 30 && imc <= 35) {
				System.out.println(" MODERADA.");
			} else if(imc > 35 && imc <= 40) {
				System.out.println(" PREMÒRBIDA.");
			} else {
				System.out.println(" MÒRBIDA.");
			}
		}
		
	}
	
}
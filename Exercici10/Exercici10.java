// Copyright (C) Xavi Álvarez. All rights reserved.

package Exercici10;

import Keyboard.*;

public class Exercici10 {

	public static void main(String[] args) {
		System.out.println("Comptador vocals majúscules");
		System.out.println("--------- ------ ----------");
		System.out.println("Escriu lletra per lletra i un punt per acabar.");
		
		char lletra = ' ';

		int a = 0;
		int e = 0;
		int i = 0;
		int o = 0;
		int u = 0;
		int r = 0;	
		int contVocal = 0;

	while(lletra != '.') {
			
			lletra = Keyboard.readChar();
			
			if(lletra == 'A') {
				a++;
			} else if(lletra == 'E') {
				e++;
			} else if(lletra == 'I') {
				i++;
			} else if(lletra == 'O') {
				o++;
			} else if(lletra == 'U') {
				u++;
			} else if (lletra == 'a' || lletra =='e' || lletra =='i' || lletra =='o' || lletra =='u') { 
				contVocal++;	
			} else if (lletra != '.') {
				r++;
					}
		}
		

		double total = (a + e + i + o + u + r + contVocal);
		double vocals = ((a + e + i + o + u + contVocal) / total) * 100;
		double consonants = (r / total) * 100;
		
		System.out.println("Vocals majúscules:");
		System.out.println("Nombre de A: " + a);
		System.out.println("Nombre de E: " + e);
		System.out.println("Nombre de I: " + i);
		System.out.println("Nombre de O: " + o);
		System.out.println("Nombre de U: " + u);
		System.out.println();
		System.out.println("Els percentatges són els seguents:");
		System.out.println("Vocals: " + vocals + " %");
		System.out.println("Consonants: " + consonants + " %");

	}

}
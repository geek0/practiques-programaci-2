// Copyright (C) Xavi Álvarez. All rights reserved.
package Exercici2;
import Keyboard.*;
import java.util.Arrays;

public class Exercici2 {
	public static void main(String[] args){
		int a, b, c;
		System.out.println("Especifíca els 3 valors");
		System.out.println("------------------------");
		System.out.println();
		System.out.println("Valor 1:");
		a = Keyboard.readInt();
		System.out.println("Valor 2:");
		b = Keyboard.readInt();
		System.out.println("Valor 3:");
		c = Keyboard.readInt();
		int[] arrnombres = {a,b,c};
		Arrays.sort(arrnombres);
		System.out.println(Arrays.toString(arrnombres));
		
	}
		
}
 
// Copyright (C) Xavi Álvarez. All rights reserved.
package Exercici3;
import Keyboard.*;

public class Exercici3 {

	public static void main(String[] args) {
		
		System.out.println("Convertidor EUROS <-> PESSETES");
		System.out.println("Entra la quantitat de diners i les unitats a convertir (\"166 P\", \"1 E\"):");
		String in = Keyboard.readString(); // m'estalvio més lectures del teclat, ho he fet de l'altre manera a l'Exercici8.
		String[] input = in.split(" ");
		double diners = Double.valueOf(input[0]);
		char unitats = input[1].charAt(0);
		
		if(unitats == 'P') {
			double E = diners/166;
			System.out.println();
			System.out.println(diners + " pessetes son " + E + " euros.");
		} else 
			if(unitats == 'E') {
			double P = diners*166;
			System.out.println();
			System.out.println(diners + " euros son " + P + " pessetes.");
		} else {
			System.out.println("No s'ha especificat bé la quantitat.");
		}
		
	}
	
}
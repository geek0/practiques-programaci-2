// Copyright (C) Xavi Álvarez. All rights reserved.
package Exercici9;
import Keyboard.*; 
public class Exercici9 {
	
	public static void main(String[] args) {
		
		System.out.println("Comptador vocals majúscules");
		System.out.println("--------- ------ ----------");
		System.out.println("Escriu lletra per lletra i un punt per acabar.");
		
		char lletra = ' ';
		int a = 0;
		int e = 0;
		int i = 0;
		int o = 0;
		int u = 0;
		
		while(lletra != '.') {
			
			lletra = Keyboard.readChar();
			
			if(lletra == 'A') {
				a++;
			} else if(lletra == 'E') {
				e++;
			} else if(lletra == 'I') {
				i++;
			} else if(lletra == 'O') {
				o++;
			} else if(lletra == 'U') {
				u++;
			
		}
		
		System.out.println("Vocals majúscules:");
		System.out.println("Nombre de A: " + a);
		System.out.println("Nombre de E: " + e);
		System.out.println("Nombre de I: " + i);
		System.out.println("Nombre de O: " + o);
		System.out.println("Nombre de U: " + u);
		
	}

}

}

		

// Copyright (C) Xavi Álvarez. All rights reserved.
package Exercici7;
import Keyboard.*;

public class Exercici7 {  
    public static void main(String[] args) {
    	
    	System.out.println("Resolució d'equacions de 2n grau");
    	System.out.println("--------- ----------- -- -- ----");
    	System.out.println("Introdueix el primer coeficient de l'equació (a)");  
        double a= Keyboard.readDouble();  
        System.out.println("Introdueix el segon coeficient de l'equació (b)");  
        double b= Keyboard.readDouble();
        System.out.println("Introdueix el tercer coeficient de l'equació (c)");  
        double c= Keyboard.readDouble(); 
        double disc=Math.pow(b,2)-4*a*c;  
        	if(a!=0){  
        		if(disc<0){  
        			System.out.println("No te solucions reals.");  
        		}
        		else {  
        			double x1=(-b+Math.sqrt(disc))/(2*a);  
        			double x2=(-b-Math.sqrt(disc))/(2*a);
        			if(x2==x1){
        			System.out.println("El resultat de l'equació és X1 = "+x1);  
        			} 
        				else{
        					System.out.println("Els resultats de l'equació són X1 = "+x1+" i X2 = "+x2); 
        				}
        			}  
      }
        else{  
      
        	System.out.println("El primer coeficient ha de ser diferent de 0.");  
      }        
      }  
}  
// Copyright (C) Xavi Álvarez. All rights reserved.
package Exercici5;
import Keyboard.*;

public class Exercici5 {

	public static void main(String[] args) {
		
		System.out.println("Classificació de l'IMC");
		System.out.println("------------- -- -----");
		System.out.print("Quin és el teu pes (en Kg.)?: ");
		double w = Keyboard.readDouble();
		System.out.print("Quina és la teva alçada (en m.)?: ");
		double h = Keyboard.readDouble();
		System.out.println();
		double imc = w / Math.pow(h, 2);
		System.out.println("El teu IMC és " + imc);
		
		if(imc < 18) {
			System.out.print("això indica INFRAPÈS");
			if(imc >= 17) {
				System.out.println(" MODERAT.");
			} else if(imc < 17 && imc >= 16) {
				System.out.println(" CONSIDERABLE.");
			} else {
				System.out.println(" SEVER.");
			}
		} else if(imc >= 18 && imc <= 25) {
			System.out.println("això vol dir que és NORMAL.");
		} else {
			System.out.print("això indica OBESITAT");
			if(imc > 25 && imc <= 30) {
				System.out.println(" LLEU.");
			} else if(imc > 30 && imc <= 35) {
				System.out.println(" MODERADA.");
			} else if(imc > 35 && imc <= 40) {
				System.out.println(" PREMÒRBIDA.");
			} else {
				System.out.println(" MÒRBIDA.");
			}
		}
		
	}
	
}
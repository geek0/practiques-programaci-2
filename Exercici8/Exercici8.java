// Copyright (C) Xavi Álvarez. All rights reserved.

package Exercici8;
import Keyboard.*;
public class Exercici8 {
	public static void main(String[] args) {
		int opcio = 0;
		while (opcio != 3) {
			System.out.println("Escolleix segons siguin euros o pessetes la quantitat que entraràs:");
			System.out.println("1.- Euros");
			System.out.println("2.- Pessetes");
			System.out.println("3.- Finalitzar");
			
			opcio = Keyboard.readInt();

			if (opcio == 1) {
				System.out.print("Entra la quantitat a convertir: ");
				double diners = Keyboard.readDouble();
				System.out.println();
				double P = diners * 166;
				System.out.println(diners + " euros son " + P + " pessetes.");
				System.out.println("---------------------------");
				System.out.println();
			} else if (opcio == 2) {
				System.out.print("Entra la quantitat a convertir: ");
				double diners = Keyboard.readDouble();
				System.out.println();
				double E = diners / 166;
				System.out.println(diners + " pessetes son " + E + " euros.");
				System.out.println("---------------------------");
				System.out.println();
			} else if (opcio > 3) { // Descarta més grans de 3
				System.out.println("Opció no vàlida.");
				System.out.println();
			}
			  else if (opcio <= 0) { // Descarta negatius
				System.out.println("Opció no vàlida.");
				System.out.println();

			}

		}

		System.out.println("Fi programa.");

		System.exit(0);

	}

}